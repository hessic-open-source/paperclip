import logging
from colorlog import ColoredFormatter

# Define custom log levels
OPEN = logging.INFO + 1
CLOSE = logging.INFO + 2
INTERESTS = logging.INFO + 3
INCREMENT = logging.INFO + 4
CLOSE_SL = logging.INFO + 5

# Add custom log levels to the logger
logging.addLevelName(OPEN, "\033[1;32mOPEN\033[0m")  # Bold green
logging.addLevelName(CLOSE, "\033[1;31mCLOSE\033[0m")  # Bold red
logging.addLevelName(INTERESTS, "\033[1;34mINTERESTS\033[0m")  # Bold blue
logging.addLevelName(INCREMENT, "\033[1;33mINCREMENT\033[0m")  # Bold yellow
logging.addLevelName(CLOSE_SL, "\033[1;35mCLOSE_SL\033[0m")  # Bold magenta


# Create custom log methods
def open_log(self, message, date, *args, **kwargs):
    self.log(OPEN, f"     {date} | {message}", *args, **kwargs)


def close_log(self, message, date, *args, **kwargs):
    self.log(CLOSE, f"    {date} | {message}", *args, **kwargs)


def interests_log(self, message, date, *args, **kwargs):
    self.log(INTERESTS, f"{date} | {message}", *args, **kwargs)


def increment_log(self, message, date, *args, **kwargs):
    self.log(INCREMENT, f"{date} | {message}", *args, **kwargs)


def close_sl_log(self, message, date, *args, **kwargs):
    self.log(CLOSE_SL, f" {date} | {message}", *args, **kwargs)


# Add custom log methods to the logger
logging.Logger.open = open_log
logging.Logger.close = close_log
logging.Logger.interests = interests_log
logging.Logger.increment = increment_log
logging.Logger.close_sl = close_sl_log

# Set up the logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Create a console handler with colorlog
console_handler = logging.StreamHandler()

# Define custom formatter
formatter = ColoredFormatter(
    # "\033[0;30m%(asctime)s | \033[0m%(levelname)s\033[0;30m | %(message)s\033[0m",
    "\033[0;30m\033[0m%(levelname)s\033[0;30m  %(message)s\033[0m",
    datefmt='%Y-%m-%d %H:%M:%S',  # Add this line
    log_colors={
        'DEBUG': 'black',
        'INFO': 'black',
        'WARNING': 'black',
        'ERROR': 'black',
        'CRITICAL': 'black',
        'INCREMENT': 'bold_yellow',
        'CLOSE_SL': 'bold_magenta',
    }
)

# Set the formatter for the console handler
console_handler.setFormatter(formatter)

# Add the console handler to the logger
logger.addHandler(console_handler)


def print_logger(type, price, balance, result, quantity, direction, timestamp):
    if type == "open":
        logger.open(f'[{float(price):.2f}] '
                    f'Balance: {float(balance):.2f}, '
                    f'Result: {float(result):.2f}, '
                    f'Qnt: {float(quantity):.2f}, '
                    f'Direction: {direction}', timestamp)

    elif type == "close":
        logger.close(
            f'[{float(price):.2f}] '
            f'Balance: {float(balance):.2f}, '
            f'Qnt: {float(quantity):.2f}, '
            f'Result: {float(result):.2f}', timestamp)
