import plotly.graph_objs as go
from paperclip.simulations.position import Direction
from plotly.subplots import make_subplots


def plot(data, subplot_keys, title='Plot', xaxis_title='Time', yaxis_title='Price', signal_colors=None,
         signal_symbols=None, signal_sizes=None, height=1000, vertical_spacing=0.10):

    if signal_colors is None:
        signal_colors = {Direction.LONG: 'green', Direction.SHORT: 'red', Direction.HOLD: 'blue'}
    if signal_symbols is None:
        signal_symbols = {Direction.LONG: 'triangle-up', Direction.SHORT: 'triangle-down', Direction.HOLD: 'circle'}
    if signal_sizes is None:
        signal_sizes = {Direction.LONG: 8, Direction.SHORT: 8, Direction.HOLD: 8}

    num_rows = len(subplot_keys) + 1
    fig = make_subplots(rows=num_rows, cols=1, shared_xaxes=True, vertical_spacing=vertical_spacing)

    timestamps = [data_point['timestamp'] for data_point in data]
    close_prices = [data_point['close'] for data_point in data]

    fig.add_trace(go.Scatter(x=timestamps,
                             y=close_prices,
                             mode='lines',
                             name='Market Data'))

    reserved_keys = {'timestamp', 'close', 'signal', 'entry', 'exit'}
    keys = set(data[0].keys()) - reserved_keys

    for idx, key in enumerate(keys):
        values = [data_point[key] for data_point in data]
        row = subplot_keys.index(key) + 2 if key in subplot_keys else 1
        fig.add_trace(go.Scatter(x=timestamps,
                                 y=values,
                                 mode='lines',
                                 name=key,
                                 connectgaps=False,
                                 hovertemplate=f'%{{y:.2f}}',
                                 text=values), row=row, col=1)

    signal_types = {Direction.LONG: 'Buy Signal', Direction.SHORT: 'Sell Signal', Direction.HOLD: 'Hold Signal'}

    for signal_type, signal_name in signal_types.items():
        signals = [data_point for data_point in data if data_point['signal'] == signal_type]
        fig.add_trace(go.Scatter(x=[signal['timestamp'] for signal in signals],
                                 y=[signal['close'] for signal in signals],
                                 mode='markers',
                                 marker=dict(color=signal_colors[signal_type], size=signal_sizes[signal_type],
                                             symbol=signal_symbols[signal_type]),
                                 name=signal_name,
                                 hovertemplate='True'), row=1, col=1)

    initiated_signals = [data_point for data_point in data if data_point['entry'] == True]
    closed_signals = [data_point for data_point in data if data_point['exit'] == True]

    fig.add_trace(go.Scatter(x=[signal['timestamp'] for signal in initiated_signals],
                            y=[signal['close'] for signal in initiated_signals],
                            mode='markers',
                            marker=dict(color='yellow', size=8, symbol='star'),
                            name='Opened position',
                            hovertemplate='%{text}',
                            text=[signal['signal'] for signal in initiated_signals]), row=1, col=1)

    fig.add_trace(go.Scatter(x=[signal['timestamp'] for signal in closed_signals],
                            y=[signal['close'] for signal in closed_signals],
                            mode='markers',
                            marker=dict(color='magenta', size=8, symbol='star'),
                            name='Closed position',
                            hovertemplate='True'), row=1, col=1)

    fig.update_layout(title=title,
                  xaxis_title=xaxis_title,
                  yaxis_title=yaxis_title,
                  xaxis_rangeslider_visible=False,
                  height=height)

    fig.update_layout(hovermode='x unified')
    fig.show()