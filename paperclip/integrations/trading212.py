import importlib
import yaml
import asyncio
import nest_asyncio
import importlib.metadata
import os
from playwright.async_api import async_playwright
from pathlib import Path
from fake_useragent import UserAgent


class Trading212:
    def __init__(self):
        # Import Trading212 config file
        try:
            package_dir = Path(importlib.metadata.distribution('hessic-paperclip').locate_file('') + '/config')
        except importlib.metadata.PackageNotFoundError:
            package_dir = Path(os.getcwd() + '/config')

        self.script_dir = package_dir
        with open(self.script_dir / 'trading212.yml') as f:
            config = yaml.safe_load(f)

        # Playwright settings
        self.playwright = None
        self.browser = None
        self.context = None
        self.page = None
        self.selectors = config['selectors']
        self.tickers = config['tickers']
        self.timeouts = config['timeouts']
        self.url = "about:blank"

        # Trading212 state
        self.logged = False

        nest_asyncio.apply()

    async def setup(self):
        self.playwright = await async_playwright().start()
        self.browser = await self.playwright.chromium.launch(headless=False, args=['--start-maximized', '--disable-blink-features=AutomationControlled'])

        user_agent = UserAgent().random
        accept_languages = "en-US,en;q=0.9"

        self.context = await self.browser.new_context(
            user_agent=user_agent,
            accept_downloads=True,
            locale="en-US",
            geolocation={"latitude": 45.4642, "longitude": 9.1900},
            permissions=["geolocation"],
            extra_http_headers={
                "Accept-Language": accept_languages,
                "sec-ch-ua": '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
                "sec-ch-ua-mobile": "?0",
            }
        )

        self.page = await self.context.new_page()

    #### Async Functions

    async def go_to_page(self, url):
        await self.page.goto(url)

    async def login_async(self, username, password):
        await self.go_to_page("https://www.trading212.com")
        if await self.page.is_visible(self.selectors['cookies_accept_button'], timeout=self.timeouts['default']):
            await self.page.click(self.selectors['cookies_accept_button'])
        await self.page.wait_for_selector(self.selectors['login_button'], timeout=self.timeouts['default'])
        await self.page.click(self.selectors['login_button'])
        await self.page.wait_for_selector(self.selectors['username_field'], timeout=self.timeouts['default'])
        await self.page.fill(self.selectors['username_field'], username)
        await self.page.wait_for_selector(self.selectors['password_field'], timeout=self.timeouts['default'])
        await self.page.fill(self.selectors['password_field'], password)
        await self.page.wait_for_selector(self.selectors['submit_button'], timeout=self.timeouts['submit_button'])
        await self.page.click(self.selectors['submit_button'])
        await self.page.click(self.selectors['submit_button'])
        await self.page.wait_for_selector(self.selectors['account_value_text'])
        if (await self.page.locator('.account-status-header-label').inner_text()) == "Account value":
            self.logged = True
            return True
        else:
            return False

    async def close(self):
        await self.browser.close()
        await self.playwright.stop()

    async def take_screenshot(self, path):
        await self.page.screenshot(path=path)

    async def reload_page_async(self):
        await self.page.reload()
        return True

    async def get_balance_async(self):
        await self.page.wait_for_selector('.account-status-header-value', state="visible")
        value = await self.page.eval_on_selector('.account-status-header-value', 'el => el.textContent')
        return value

    async def get_blocked_funds_async(self):
        await self.page.wait_for_selector('div[data-qa-status-bar-item="status-bar-item-blocked-funds"] .value',
                                          state="visible")
        value = await self.page.eval_on_selector('div[data-qa-status-bar-item="status-bar-item-blocked-funds"] .value',
                                                 'el => el.textContent')
        return value

    async def get_free_funds_async(self):
        await self.page.wait_for_selector(f'div[data-qa-status-bar-item="status-bar-item-free-funds"] .value',
                                          state="visible")
        value = await self.page.eval_on_selector(f'div[data-qa-status-bar-item="status-bar-item-free-funds"] .value',
                                                 'el => el.textContent')
        return value

    #### Sync wrappers

    def login(self, username, password):
        async def login_async():
            await self.setup()
            await self.login_async(username, password)

        asyncio.get_event_loop().run_until_complete(login_async())

    def get_balance(self):
        async def run_get_balance():
            return await self.get_balance_async()

        return asyncio.get_event_loop().run_until_complete(run_get_balance())

    def get_blocked_funds(self):
        async def run_get_blocked_funds():
            return await self.get_blocked_funds_async()

        return asyncio.get_event_loop().run_until_complete(run_get_blocked_funds())

    def get_free_funds(self):
        async def run_get_free_funds():
            return await self.get_free_funds_async()

        return asyncio.get_event_loop().run_until_complete(run_get_free_funds())

    def reload(self):
        async def run_reload_page():
            return await self.reload_page_async()

        return asyncio.get_event_loop().run_until_complete(run_reload_page())