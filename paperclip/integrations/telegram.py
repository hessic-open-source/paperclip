import nest_asyncio
import asyncio
import telegram
import logging


class Telegram:
    def __init__(self, token, chat_id):
        self.token = token
        self.chat_id = chat_id
        nest_asyncio.apply()

    async def _send_message(self, message):
        original_logging_level = logging.getLogger().level
        logging.getLogger().setLevel(logging.CRITICAL)
        bot = telegram.Bot(token=self.token)
        await bot.send_message(chat_id=self.chat_id, text=message, parse_mode='Markdown')
        logging.getLogger().setLevel(original_logging_level)

    def send_message(self, message):
        asyncio.run(self._send_message(message))