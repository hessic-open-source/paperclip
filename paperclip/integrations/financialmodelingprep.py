import requests
import polars as pl
from datetime import datetime, timedelta

class FinancialModelingPrep:
    def __init__(self, api_key):
        self.api_key = api_key

    def fetch_stock_data(self, symbol, days_back=10):
        """
        Fetches historical stock data for a given symbol over a specified number of days.

        Args:
            symbol (str): Stock symbol for which to fetch data.
            days_back (int): Number of days in the past to retrieve data for. Defaults to 10.

        Returns:
            pl.DataFrame: A dataframe containing the historical stock data without duplicates.

        Raises:
            Exception: If an error occurs during the data fetching process.
        """
        end_date = datetime.now()
        start_date = end_date - timedelta(days=days_back)
        data = []

        current_date = start_date
        while current_date <= end_date:
            date_str = current_date.strftime('%Y-%m-%d')
            url = f'https://financialmodelingprep.com/api/v3/historical-chart/1min/{symbol}?from={date_str}&to={date_str}&apikey={self.api_key}'
            response = requests.get(url)
            if response.status_code != 200:
                raise Exception(f"Error fetching data for {date_str}: {response.status_code}")
            day_data = response.json()
            data.extend(day_data)
            current_date += timedelta(days=1)

        # Create DataFrame from the data
        data_frame = pl.DataFrame(data)

        # Ensure that the 'date' column is interpreted as a datetime
        data_frame = data_frame.with_columns(pl.col('date').str.strptime(pl.Datetime, "%Y-%m-%d %H:%M:%S").alias("timestamp"))

        # Remove duplicates based on 'timestamp' column
        data_frame = data_frame.unique(["timestamp"])

        # Sort the DataFrame based on the timestamp
        data_frame = data_frame.sort("timestamp")

        # Add a column for the symbol
        data_frame = data_frame.with_columns(pl.lit(symbol).alias("symbol"))

        # Remove the 'date' column
        data_frame = data_frame.drop("date")

        return data_frame.select(["timestamp"] + [col for col in data_frame.columns if col != "timestamp"])