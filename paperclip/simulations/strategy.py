import traceback
from abc import ABC, abstractmethod
from paperclip.simulations.position import Position
from paperclip.utils.logging import print_logger
from polars import DataFrame


class Strategy(ABC):
    def __init__(self, data: DataFrame):
        self.data = data.sort("timestamp")

        # strategy
        self.signal = None
        self.data_point = None
        self.initial_balance = None
        self._realized_balance = 0

        # position
        self.position = None

    def __str__(self) -> str:
        attributes = [f"{key}: {value}" for key, value in vars(self).items()]
        return "\n".join(attributes)

    def reset_position(self):
        self.position = None

    @property
    def balance(self) -> float:
        if self.position:
            return self.initial_balance + self._realized_balance + self.position.result
        else:
            return self.initial_balance + self._realized_balance

    @property
    def free_funds(self) -> float:
        return self.balance - self.blocked_funds

    @property
    def blocked_funds(self) -> float:
        if self.position:
            return self.position.margin_rate * self.position.quantity * self.position.avg_buying_price
        else:
            return 0

    @property
    def margin_status(self) -> float:
        if self.position:
            return (self.free_funds + self.blocked_funds) / (self.free_funds + 2 * self.blocked_funds) * 100
        else:
            return 100

    @abstractmethod
    def _preprocess(self):
        pass

    @abstractmethod
    def _quantity(self) -> float:
        pass

    @abstractmethod
    def _failed(self) -> bool:
        pass

    @abstractmethod
    def _exit(self) -> bool:
        pass

    @abstractmethod
    def _direction(self) -> str:
        pass

    @abstractmethod
    def _entry(self) -> bool:
        pass

    @abstractmethod
    def _custom(self):
        pass

    @abstractmethod
    def _success(self):
        pass

    def update_state(self):
        if self.position:
            self.position.update(self.data_point)

    def run(self):
        try:
            # preprocess data
            self._preprocess()

            for row in self.data.rows(named=True):
                self.data_point = row

                # update state
                self.update_state()

                # custom function execution
                self._custom()

                # strategy failure check
                if self._failed(): break

                # direction calculation
                self._direction()

                # exit/entry position handling
                self.handle_position_exit()
                self.handle_position_entry()

            # close remaining positions
            self.close_remaining_position()

            # success/failure check
            if not self._failed():
                self._success()

        except Exception as e:
            print(traceback.print_exc())
            pass

    def handle_position_exit(self):
        if self.position is not None and self._exit():
            print_logger(type="close", price=self.data_point['close'], balance=self.balance,
                         result=self.position.result, quantity=self.position.quantity,
                         direction=self.position.direction, timestamp=self.data_point['timestamp'])
            self._realized_balance += self.position.result
            self.reset_position()

    def handle_position_entry(self):
        if self.position is None and self._entry():
            self.position = Position(price=self.data_point['close'],
                                     quantity=self._quantity(),
                                     direction=self.signal)
            print_logger(type="open", price=self.position.entry_price, balance=self.balance,
                         result=self.position.result, quantity=self.position.quantity,
                         direction=self.position.direction, timestamp=self.data_point['timestamp'])

    def close_remaining_position(self):
        if self.position is not None:
            print_logger(type="close", price=self.data_point['close'], balance=self.balance,
                         result=self.position.result, quantity=self.position.quantity,
                         direction=self.position.direction, timestamp=self.data_point['timestamp'])
            self.reset_position()
