import unittest
from paperclip.simulations.position import Position, Direction


class TestPosition(unittest.TestCase):

    def test_init(self):
        pos = Position(price=100, quantity=10, direction=Direction.LONG, symbol="AAPL", leverage=2, short_interest=0.01,
                       long_interest=0.02, spread_factor=0.001, margin_rate=0.05)
        self.assertEqual(pos._price, 100)
        self.assertEqual(pos.price, 100.1)
        self.assertEqual(pos.quantity, 10)
        self.assertEqual(pos.direction, Direction.LONG)
        self.assertEqual(pos.entry_price, 100.1)
        self.assertEqual(pos.avg_buying_price, 100.1)
        self.assertEqual(pos.result, 0)
        self.assertEqual(pos.realized_result, 0)
        self.assertEqual(pos.total_cost, 1001)
        self.assertEqual(pos.symbol, "AAPL")
        self.assertEqual(pos.leverage, 2)
        self.assertEqual(pos.short_interest, 0.01)
        self.assertEqual(pos.long_interest, 0.02)
        self.assertEqual(pos.margin_rate, 0.05)
        self.assertEqual(pos.spread_factor, 0.001)

    def test_init2(self):
        pos = Position(price=100, quantity=10, direction=Direction.SHORT)
        self.assertEqual(pos._price, 100)
        self.assertEqual(pos.price, 100)
        self.assertEqual(pos.quantity, 10)
        self.assertEqual(pos.direction, Direction.SHORT)
        self.assertEqual(pos.entry_price, 100)
        self.assertEqual(pos.avg_buying_price, 100)
        self.assertEqual(pos.result, 0)
        self.assertEqual(pos.realized_result, 0)
        self.assertEqual(pos.total_cost, 1000)
        self.assertEqual(pos.symbol, None)
        self.assertEqual(pos.leverage, 1)
        self.assertEqual(pos.short_interest, 0)
        self.assertEqual(pos.long_interest, 0)
        self.assertEqual(pos.margin_rate, 0)
        self.assertEqual(pos.spread_factor, 0)

    def test_update(self):
        pos = Position(100, 10, Direction.LONG)
        pos.update_price(110)
        self.assertEqual(pos._price, 110)

    def test_price(self):
        pos = Position(100, 10, Direction.LONG, spread_factor=0.001)
        self.assertEqual(pos.price, 100.1)
        pos.direction = Direction.SHORT
        self.assertEqual(pos.price, 99.9)

    def test_result(self):
        pos = Position(100, 10, Direction.LONG, spread_factor=0)
        pos.update_price(110)
        self.assertEqual(pos.result, 100)
        pos.direction = Direction.SHORT
        pos.update_price(90)
        self.assertEqual(pos.result, 100)

    def test_edit_increment(self):
        pos = Position(100, 10, Direction.LONG, spread_factor=0.001)
        pos.edit("increment", 5)
        self.assertEqual(pos.quantity, 15)
        self.assertEqual(pos.total_cost, 1501.5)
        self.assertEqual(pos.avg_buying_price, 100.1)

    def test_edit_decrement(self):
        pos = Position(100, 10, Direction.LONG, spread_factor=0)
        pos.update_price(110)
        pos.edit("decrement", 5)
        self.assertEqual(pos.realized_result, 50)
        self.assertEqual(pos.quantity, 5)


if __name__ == "__main__":
    unittest.main()
