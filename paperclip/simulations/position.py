from enum import Enum
from typing import Optional


class Direction(Enum):
    LONG = "long"
    SHORT = "short"
    HOLD = "hold"


class Position:
    def __init__(self,
                 price: float,
                 quantity: float,
                 direction: Direction,
                 symbol: Optional[str] = None,
                 short_interest: float = 0,
                 long_interest: float = 0,
                 spread_factor: float = 0,
                 margin_rate: float = 0):
        """
        Initialize a new Position object.
        """
        self._price = price
        self.quantity = quantity
        self.direction = direction
        self.symbol = symbol
        self.short_interest = short_interest
        self.long_interest = long_interest
        self.spread_factor = spread_factor
        self.margin_rate = margin_rate
        self.entry_price = self.price
        self.avg_buying_price = self.entry_price
        self.realized_result = 0

    def __str__(self) -> str:
        attributes = [f"{key}: {value}" for key, value in vars(self).items()]
        return "\n".join(attributes)

    @property
    def price(self) -> float:
        """
        Calculate the adjusted price based on the spread factor and direction.
        """
        if self.direction == Direction.LONG:
            return self._price + self.spread_factor * self._price
        else:
            return self._price - self.spread_factor * self._price

    @property
    def result(self) -> float:
        """
        Calculate the current result of the position.
        """
        if self.direction == Direction.LONG:
            return (self.price - self.avg_buying_price) * self.quantity
        elif self.direction == Direction.SHORT:
            return (self.avg_buying_price - self.price) * self.quantity

    @property
    def total_cost(self) -> float:
        """
        Calculate the total cost of the position.
        """
        return self.quantity * self.price

    def update(self, data_point):
        """
        Update the price of the position.
        """
        self._price = data_point['close']

    def update_price(self, price):
        """
        Update the price of the position.
        """
        self._price = price

    def edit(self, action: str, quantity: int):
        """
        Edit the position based on the action and quantity provided.
        """
        if action == "increment":
            self.quantity += quantity
            self.avg_buying_price = self.total_cost / self.quantity

        elif action == "decrement":
            if self.direction == Direction.LONG:
                realized_result = (self.price - self.avg_buying_price) * quantity
            else:
                realized_result = (self.avg_buying_price - self.price) * quantity

            self.realized_result += realized_result
            self.quantity -= quantity
        else:
            raise ValueError("Invalid action. Use 'increment' or 'decrement'.")