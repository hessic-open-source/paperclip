from setuptools import setup, find_packages

with open("requirements.txt") as f:
    requirements = f.read().splitlines()

setup(
    name="hessic_paperclip",
    version="0.1.0",
    packages=find_packages(),
    include_package_data=True,
    install_requires=requirements,
    author="Kai Wen Cui",
    author_email="kaiwencui@outlook.com",
    description="Paperclip",
    long_description=open("README.md").read(),
    url="https://gitlab.com/hessic-open-source/paperclip",
)