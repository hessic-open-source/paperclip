![Paperclip Logo](https://gitlab.com/hessic-open-source/paperclip/-/raw/main/logo.png)

--------------------------------------------------------------------------------

_paperclip_ is a Python backtesting framework that offers modularity and customizability for the analysis and evaluation of trading strategies using both historical and real-time data. It uniquely supports explicit iteration, allowing for detailed, step-by-step analysis of trading strategies, particularly beneficial for complex, stateful strategies that require granular control over each data point.

Leveraging the performance of Polars, Paperclip ensures efficient handling of large datasets without compromising on speed. This makes it a practical solution for users who need the flexibility of explicit iteration combined with robust data processing capabilities.

# Installation

Before installing paperclip, ensure Python 3.8 or higher is installed on your system. You can check your Python version by running `python --version` in your command line.

### Install paperclip via Git

You can install the latest version of paperclip directly from the Git repository using pip. Run the following command in your terminal:

```bash
pip install git+https://gitlab.com/hessic-open-source/paperclip.git
```

This command will download and install paperclip along with its dependencies.

# API Support
_paperclip_  supports various APIs for enhanced functionality. Below is a table listing the supported APIs:

<table style="width: 500px;">
  <thead>
    <tr>
      <th>API</th>
      <th>Description</th>
      <th>Official Support</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Telegram</td>
      <td>Enables notifications for real-time trading</td>
      <td>[python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot)</td>
    </tr>
    <tr>
      <td>FinancialModelingPrep</td>
      <td>Market Data Fetching</td>
      <td>[Documentation](https://site.financialmodelingprep.com/developer/docs)</td>
    </tr>
  </tbody>
</table>

## License

This software ("paperclip") is licensed under the GNU General Public License (GPL). See the [LICENSE](LICENSE) file in this repository for the full license text.

## Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Contributing

Contributions to this project are welcome. By contributing, you agree that your contributions will be licensed under its GNU General Public License (GPL).

## Contact Information

Kai Wen Cui (kaiwencui@outlook.com)

## More Information

For more information about the GNU General Public License (GPL), please visit [GNU Operating System](https://www.gnu.org/licenses/gpl-3.0.html).
